# SUMMARY

This module allows you to create an XML feed from views of a Drupal site. 
You can create any type of XML feed.

For a full description visit the project page:
https://www.drupal.org/project/xmlfeedviews

# REQUIREMENTS

*None. (Other than a clean Drupal 8 installation)

# INSTALLATION

Install as usual. We advise using composer for this:
`composer require drupal/xmlfeedviews`

Cleaning the cache after installation may be required.

#  SAMPLE VIEW

After the installation, go to Structure > Views and find "XML Feed Views". 
This is one of the samples where you can see the sitemap XML.
